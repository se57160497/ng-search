import {Component, OnInit} from '@angular/core';
import {LoaderService} from '../../../services/utils/loader.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(public loader: LoaderService) {
  }

  ngOnInit() {
  }

}
