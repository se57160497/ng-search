import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {RestaurantService} from '../../../../../services/restaurant/restaurant.service';
import {LoaderService} from '../../../../../services/utils/loader.service';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {

  constructor(private fb: FormBuilder, private restaurantService: RestaurantService, private loader: LoaderService) {
  }

  searchForm = this.fb.group({
    keyword: ['', [Validators.required]]
  });
  result = [];

  ngOnInit() {
    this.onSearch();
  }

  onSearch() {
    this.loader.OnLoading();
    this.restaurantService.getPlaceNearByBangsue(null)
      .subscribe((res) => {
        this.result = res.results;
        this.loader.OffLoading();
      });
  }
}
