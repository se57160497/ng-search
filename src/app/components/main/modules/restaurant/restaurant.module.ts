import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RestaurantRoutingModule} from './restaurant-routing.module';
import {RestaurantComponent} from './core/restaurant.component';
import {
  NZ_ICONS,
  NzButtonModule,
  NzCardModule,
  NzEmptyModule,
  NzGridModule,
  NzIconModule,
  NzInputModule,
  NzRateModule,
  NzTableModule
} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RestaurantService} from '../../../../services/restaurant/restaurant.service';
import {IconDefinition} from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};

const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => {
  const i = antDesignIcons[key];
  return i;
});

@NgModule({
  declarations: [RestaurantComponent],
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    NzGridModule,
    NzTableModule,
    ReactiveFormsModule,
    NzCardModule,
    FormsModule,
    NzRateModule,
    NzEmptyModule
  ],
  exports: [NzIconModule],
  providers: [RestaurantService]
})
export class RestaurantModule {
}
