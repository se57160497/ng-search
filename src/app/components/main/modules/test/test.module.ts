import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { TestComponent } from './core/test.component';
import {NzButtonModule, NzGridModule, NzInputModule, NzToolTipModule} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [TestComponent],
  imports: [
    CommonModule,
    TestRoutingModule,
    NzGridModule,
    NzToolTipModule,
    NzInputModule,
    FormsModule,
    NzButtonModule
  ]
})
export class TestModule { }
