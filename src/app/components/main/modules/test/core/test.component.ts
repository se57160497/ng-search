import {Component, OnInit} from '@angular/core';
import {TestService} from '../../../../../services/test/test.service';
import {LoaderService} from '../../../../../services/utils/loader.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  Input = ['X', 5, 9, 15, 23, 'Y', 'Z'];
  result = [];

  constructor(private testService: TestService, private loader: LoaderService) {
  }

  ngOnInit() {
  }

  onFinding() {
    this.loader.OnLoading();
    this.testService.getFindingXYZ({question: this.Input})
      .subscribe((res) => {
        console.log(res);
        if (res !== null && res !== undefined) {
          this.result = Object.keys(res).map((data => res[data]));
        }
        this.loader.OffLoading();
      });
  }

  onValueChange(event, index) {
    this.Input[index] = event.srcElement.value;
  }

}
