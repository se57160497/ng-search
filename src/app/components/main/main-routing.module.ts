import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from './core/main.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {path: '', redirectTo: 'restaurant', pathMatch: 'full'},
      {path: 'restaurant', loadChildren: () => import('./modules/restaurant/restaurant.module').then(mod => mod.RestaurantModule)},
      {path: 'test', loadChildren: () => import('./modules/test/test.module').then(mod => mod.TestModule)},
      {path: 'resume', loadChildren: () => import('./modules/resume/resume.module').then(mod => mod.ResumeModule)},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
