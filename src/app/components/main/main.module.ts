import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './core/main.component';
import {NzBreadCrumbModule, NzLayoutModule, NzMenuModule, NzSpinModule} from 'ng-zorro-antd';
import { HeaderComponent } from './core/pages/header/header.component';
import { FooterComponent } from './core/pages/footer/footer.component';


@NgModule({
  declarations: [MainComponent, HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzMenuModule,
    NzSpinModule
  ]
})
export class MainModule { }
