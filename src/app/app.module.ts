import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgZorroAntdModule, NZ_I18N, en_US} from 'ng-zorro-antd';
import {AppComponent} from './app.component';
/** config angular i18n **/
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {RouterModule, Routes} from '@angular/router';

registerLocaleData(en);

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/main/main.module').then(mod => mod.MainModule),
  }
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgZorroAntdModule,
    RouterModule.forRoot(
      appRoutes,
    )
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: NZ_I18N, useValue: en_US}
  ]
})
export class AppModule {
}
