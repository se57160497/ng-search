import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  isLoading = false;

  constructor() {
  }

  OnLoading() {
    this.isLoading = true;
  }

  OffLoading() {
    this.isLoading = false;
  }
}
