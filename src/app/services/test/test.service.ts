import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;charset=UTF-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type'
    }),
    withCredentials: true,
  };

  constructor(private http: HttpClient) {
  }

  getFindingXYZ(body: any) {
    return this.http.post<any>(environment.service_url + environment.FINDING_XYZ, body, this.httpOptions);
  }
}
